import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import './App.css';
import Home from './Pages/Home';
import Courses from './Pages/Courses';

function App() {
  return (
    <div>
      <AppNavBar />
      <Container>
        <Home />
        <Courses />
      </Container>
    </div>
  );
}

export default App;
